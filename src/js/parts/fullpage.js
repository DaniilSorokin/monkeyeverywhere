//import $ from "jquery";
import fullpage from 'fullpage.js';

// When using fullPage extensions replace the previous import
// of fullpage.js for this file
//import fullpage from 'fullpage.js/dist/fullpage.extensions.min';

// Initializing it
var fullPageInstance = new fullpage('#fullpage', {
    navigation: false,
    sectionsColor:['#ffffff'],
    afterLoad: function(origin, dist){
        //console.log(dist)
        //$('.fixed').css({'color' : dist.item.dataset.sectionColor})
        $('.text-block_text').addClass('active')
        if (dist.isLast == true) {
            $('.fixed').css({'opacity' : '0'})
            $('.heading-block--main').addClass('heading-block--last')
        } else {
            $('.fixed').css({'color' : dist.item.dataset.sectionColor, 'opacity' : '1'})
            $('.heading-block--main').removeClass('heading-block--last')
        }
           
        
    },
    scrollingSpeed: 1000,
    onLeave: function(origin, destination, direction){
        $('.text-block_text').removeClass('active')
    },

}); 


