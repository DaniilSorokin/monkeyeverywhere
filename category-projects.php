


<?php get_header(); ?>
<?php if(have_posts()): ?>
    <section class="projects">
        <div class="list">
            <?php while(have_posts()):the_post(); ?>
            <a href="<?php the_permalink(); ?>" class="list-item">
                <div class="list-item_img">
                    <?php the_post_thumbnail(); ?>
                </div>
                <p class="text-block_text text-block_text--projects"><?php the_title(); ?> <span class="color-text" style="color:<?php the_field('color') ?>"><?php the_field('description'); ?></span></p>
            </a>
            <?php endwhile; ?>
        </div>
    </section>
<?php endif; ?>
<?php wp_footer(); ?>