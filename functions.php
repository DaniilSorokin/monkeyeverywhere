<?php

add_theme_support('post-thumbnails');

register_nav_menus(array(
    'top'    => 'Top menu'
));

// WP FILTER FUNCTIONS //
function new_excerpt_more($more)
{
    return '...';
}
function mytheme_custom_excerpt_length($length)
{
    return 12;
}
add_filter('excerpt_more', 'new_excerpt_more');
add_filter('excerpt_length', 'mytheme_custom_excerpt_length', 999);
add_image_size( 'custom_thumb', 32, 32, true );
////////////////////////////////
add_filter('wpcf7_autop_or_not', '__return_false');
if (function_exists('acf_add_options_page')) {

    $option_page = acf_add_options_page(
        array(
            'page_title' => 'Контактные данные',
            'menu_title' => 'Контактные данные',
            'menu_slug'  => 'contacts',
            'capability' => 'edit_posts',
            'redirect'   => false
        )
    );
    $option_page = acf_add_options_page(
        array(
            'page_title' => 'Настройки сайта',
            'menu_title' => 'Настройки сайта',
            'menu_slug'  => 'options',
            'capability' => 'edit_posts',
            'redirect'   => false
        )
    );
}

// login/register
// include 'func/auth/func-login.php';
// include 'func/auth/func-register.php';
// include 'func/auth/func-global-auth.php';

// profile
// include 'func/profile/update-profile.php';
// include 'func/profile/delete-post.php';

// create aid
// include 'func/create-aid/func-aid.php';
// include 'func/create-aid/update-aid.php';
// include 'func/create-aid/edit-aid.php';

// create comment
// include 'func/create-comment.php';
// include 'func/like-payment.php';
// actions 



// endpoints
// include 'func/endpoints/register-rest-cp.php';

// admin
//include 'func/admin-filter.php';

// register-blocks
// include 'func/register-blocks.php';

// автообновление версии файлов

function my_theme_load_resources()
{

    $theme_uri = get_template_directory_uri();
    $theme_styles = $theme_uri . '/dist/css/style.bundle.css';
    $theme_scripts = $theme_uri . '/dist/js/bundle.js';


    // global style connected

    wp_register_style('my-theme-style', $theme_styles, false, filemtime(get_stylesheet_directory() . '/dist/css/style.bundle.css'));
    wp_enqueue_style('my-theme-style');

    // scripts connected

    wp_register_script('my_theme_functions', $theme_scripts, array('jquery'), filemtime(get_stylesheet_directory() . '/dist/css/style.bundle.css'), true);
    wp_enqueue_script('my_theme_functions');
}

add_action('wp_enqueue_scripts', 'my_theme_load_resources');


add_action('wp_footer', 'mycustom_wp_footer');

function mycustom_wp_footer()
{
?>
  <script type="text/javascript">
    function noteForm() {
      setTimeout(function() {
        jQuery('#note_form .note_form-text').text(jQuery('.wpcf7-response-output').text());
        jQuery('#note_form').addClass('open');
      }, 500);

      setTimeout(function() {
        jQuery('#note_form').removeClass('open');

      }, 5500);
    }
    document.addEventListener('wpcf7mailsent', function(event) {
      noteForm();
    }, false);
    document.addEventListener('wpcf7invalid', function(event) {
      noteForm();
    }, false);
    document.addEventListener('wpcf7spam', function(event) {
      noteForm();
    }, false);
    document.addEventListener('wpcf7mailfailed', function(event) {
      noteForm();
    }, false);
    document.addEventListener('wpcf7submit', function(event) {
      noteForm();

    }, false);
  </script>
<?php
}
