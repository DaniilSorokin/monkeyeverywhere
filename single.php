<?php get_header(); ?>

<div class="single-post">
    <div class="single-post_img">
        <?php the_post_thumbnail(); ?>
        <p class="text-block_text text-block_text--projects text-block_text--single"><?php the_title(); ?> <span class="color-text" style="color:<?php the_field('color') ?>"><?php the_field('description'); ?></span></p>
    </div>
    <?php $img = get_field('logo');
    if ($img) { ?>
    <div class="content-wrap">
        <div class="content-img">
            <img src="<?php the_field('logo'); ?>">
        </div>
    </div>
    <div class="gutenberg-wrap">
        <?php the_content(); ?>
    </div>
    <?php } else { ?>
    <div class="gutenberg-wrap">
        <?php the_content(); ?>
    </div>
    <?php } ?>
</div>

<?php wp_footer(); ?>