<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta charset="UTF-8">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?> >
    <?php if(is_front_page()): ?>
        <span class="fixed">Everywhere.</span>
    <?php endif; ?>
    <?php if( wp_is_mobile() ){ ?>
        <header class="header-mob">
            <div class="header-mob_block">
                <a href="/" class="header-item header-item--logo">
                    <span class="rotate-item">M.</span><span>e.</span>
                    <!-- <img src="dist/img/me.png"> -->
                </a>
                <div class="burger"></div>
                <div class="burger-items">
                    <div class="close"></div>
                    <div class="burger-items_wrap dis-flex justify-content-center align-items-center">
                        <div class="burger-item">
                            <a href="<?php echo get_term_link(2); ?>" class="navigation-link navigation-link--projects">
                                <span data-text="Projects">
                                    Projects
                                </span> 
                            </a>
                        </div>
                        <?php if( is_category(2)) {?>
                            <div class="burger-item">
                                <a href="https://dev.monkeyeverywhere.com/#contact" class="navigation-link navigation-link--contact">
                                    <span data-text="Contact">Contact</span>
                                </a>
                            </div> 
                        <?php } else { ?>
                            <div class="burger-item">
                                <a href="#contact" class="navigation-link navigation-link--contact">
                                    <span data-text="Contact">Contact</span>
                                </a>
                            </div> 
                    
                        <?php } ?>
                    </div>
                </div>
            </div>
        </header>

    <?php } else { ?>
    <header class="main-header">
        <div class="header-block">
            <a href="/" class="header-item header-item--logo">
                <span class="rotate-item">M.</span><span>e.</span>
                <!-- <img src="dist/img/me.png"> -->
            </a>
            <div class="header-item header-item--projects">
                <a href="<?php echo get_term_link(2); ?>" class="navigation-link navigation-link--projects">
                    <span data-text="Projects">
                        Projects
                    </span> 
                </a>
            </div>
            <?php if( is_category(2)) {?>
                <div class="header-item header-item--contact">
                    <a href="http://dev.monkey.ru/#contact" class="navigation-link navigation-link--contact">
                        <span data-text="Contact">Contact</span>
                    </a>
                </div> 
            <?php } else { ?>
                <div class="header-item header-item--contact">
                    <a href="#contact" class="navigation-link navigation-link--contact">
                        <span data-text="Contact">Contact</span>
                    </a>
                </div> 
            <?php } ?>
        </div>
    </header>
    <?php } ?>
    

