<?php
/*
Template Name: Front page
*/
?>
<?php get_header(); ?>

<?php if(have_rows('front_slides')): ?>
<?php $slides_count = 0; ?>
<div id="fullpage">
    <?php while (have_rows('front_slides')): the_row(); $color = get_sub_field('color');?>
    <div data-anchor="<?php the_sub_field('nav') ?>" data-section-color="<?php echo $color['value'];?>" class="section section--<?php echo $color['label'];?>">
        <div class="section-wrap col-lg-12">
            <div class="heading-block heading-block--main">
                <!-- <?php if($slides_count == 0 || $slides_count == 8) { ?>
                <div class="main-block_logo main-block_logo--first">
                    <span class="logo-item">M.</span>e.
                </div>
                <?php } ?> -->
                <h1 data-text="<?php the_sub_field('under_title') ?>." class="heading-block_title heading-block_title--main"><?php the_sub_field('title'); ?></h1>
            </div>
            <div class="text-block col-lg-5 col-xs-12">
                <p class="text-block_text">
                    <?php the_sub_field('text'); ?>
                </p>
                
            </div>
            <div class="text-block text-block--others">
                <p class="text-block_text text-block_text--others">
                    <?php the_sub_field('other'); ?>
                </p>
                <?php if($slides_count == 7) { ?>
                    <a href="mailto: Banana@monkeyeverywhere.com" class="text-block_text text-block_text--others">
                        Banana@monkeyeverywhere.com
                    </a> 
                <?php } else { ?>
                <p class="text-block_text text-block_text--others">
                    <?php the_sub_field('right'); ?>
                </p>
                <?php } ?>
            </div>
        </div>
    </div>
    <?php $slides_count++; ?>
    <?php endwhile; ?>
</div>
<?php endif; ?>
<?php get_footer(); ?>